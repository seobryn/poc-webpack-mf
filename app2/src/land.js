import Heading from './components/heading/heading';
import LandImg from './components/land-image/land-image';

new Heading().render(document.body,'Land');
new LandImg().render(document.body);
