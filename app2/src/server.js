const express = require('express');
const app = express()
const path = require('path')
const fs = require('fs')

const PORT = 8082;

function GetFile (fileName) {
    const filePath = path.resolve(__dirname,`../dist/${fileName}.html`);
    const htmlText = fs.readFileSync(filePath,'utf-8');
    return htmlText;
}

app.get('/', (req,res)=>{
    const fileContent = GetFile('land');
    res.send(fileContent);
});

app.use('/', express.static(path.resolve(__dirname, '../dist')))

app.listen(PORT, ()=>{
    console.log(`App Running on Port ${PORT}`);
})