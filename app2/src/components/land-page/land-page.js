import Heading from '../heading/heading';
import LandImg from '../land-image/land-image';

export default class LandPage {
  render(rootId) {
    const container = document.getElementById(rootId);
    new Heading().render(container,'Land');
    new LandImg().render(container);
  }
}
