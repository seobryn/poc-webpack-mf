import landImg from './land.jpeg'
import './land-image.scss'

export default class LandImg {
    render(parentElement){
        const landImgElement = document.createElement('img');
        landImgElement.src=landImg;
        landImgElement.alt="Land Img";
        landImgElement.classList.add('land-img')
        parentElement.appendChild(landImgElement);
    }
}