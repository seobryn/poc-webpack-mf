const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { ModuleFederationPlugin } = require('webpack').container;

const isDevelopment = process.env.NODE_ENV !== 'production';

const plugins = !isDevelopment
  ? [
      new MiniCssExtractPlugin({
        filename: '[name].[contenthash].css',
      }),
    ]
  : [];

plugins.push(new CleanWebpackPlugin());
plugins.push(
  new HtmlWebpackPlugin({
    template: 'page-template.hbs',
    filename: 'land.html',
    title: 'App2',
    inject: 'body',
    scriptLoading: 'blocking',
    minify: !isDevelopment,
  })
);

plugins.push(
  new ModuleFederationPlugin({
    name: 'LandApp',
    filename: 'remoteEntry.js',
    exposes: {
      './LandPage': './src/components/land-page/land-page.js'
    }
  })
);


module.exports = {
  entry: './src/land.js',
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, './dist'),
    publicPath: 'http://localhost:8082/',
  },
  mode: isDevelopment ? 'development' : 'production',
  devServer: {
    contentBase: path.resolve(__dirname, './dist'),
    index: 'land.html',
    port: 8082,
    writeToDisk: true,
  },
  module: {
    rules: [
      {
        test: /\.(png|jpg|jpeg)$/,
        type: 'asset/resource',
      },
      {
        test: /.(s)?css$/,
        use: [
          isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/env'],
            plugins: ['@babel/plugin-proposal-class-properties'],
          },
        },
      },
      {
        test: /.hbs$/,
        use: ['handlebars-loader'],
      },
    ],
  },
  optimization: {
    minimize: true,
    splitChunks: {
      chunks: 'all',
      minSize: 3000,
    },
  },
  plugins,
};
