const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { ModuleFederationPlugin } = require('webpack').container;

const isDevelopment = process.env.NODE_ENV !== 'production';

const plugins = !isDevelopment
  ? [
      new MiniCssExtractPlugin({
        filename: '[name].[contenthash].css',
      }),
    ]
  : [];

plugins.push(new CleanWebpackPlugin());
plugins.push(
  new HtmlWebpackPlugin({
    template: 'page-template.hbs',
    filename: 'index.html',
    title: 'App1',
    inject: 'body',
    scriptLoading: 'blocking',
    minify: !isDevelopment,
  })
);
plugins.push(
  new ModuleFederationPlugin({
    name: 'MainApp',
    filename:'remoteEntry.js',
    exposes: {
      './TestButton':'./src/components/test-button/test-button.js',
      './MainPage': './src/components/main-page/main-page.js'
    }
  })
);
module.exports = {
  entry: './src/main.js',
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, './dist'),
    publicPath: 'http://localhost:8081/',
  },
  mode: isDevelopment ? 'development' : 'production',
  devServer: {
    contentBase: path.resolve(__dirname, './dist'),
    index: 'index.html',
    port: 8081,
    writeToDisk: true,
  },
  module: {
    rules: [
      {
        test: /.(s)?css$/,
        use: [
          isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/env'],
            plugins: ['@babel/plugin-proposal-class-properties'],
          },
        },
      },
      {
        test: /.hbs$/,
        use: ['handlebars-loader'],
      },
    ],
  },
  optimization: {
    minimize: true,
    splitChunks: {
      chunks: 'all',
      minSize: 3000,
    },
  },
  plugins,
};
