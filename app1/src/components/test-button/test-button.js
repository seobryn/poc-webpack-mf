import './test-button.scss'
class TestButton {

  buttonText ="Hello World"

  render(parentElement) {
      const button = document.createElement('button');
      button.innerHTML = this.buttonText;
      button.classList.add('test-button');

      button.onclick = ()=>{
        const p = document.createElement('p');
        p.innerHTML = "My Paragraph";
        p.classList.add('test-paragraph')
        parentElement.appendChild(p);
      }

      parentElement.appendChild(button);
  }
}

export default TestButton;
