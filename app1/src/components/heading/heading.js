import './heading.scss'
import { upperFirst } from 'lodash';

export default class  Heading {
    render(parentElement,pageName){
        const headingTag = document.createElement('h1');
        headingTag.innerText=`We Are On -> ${upperFirst(pageName)}`;
        headingTag.classList.add('heading');

        parentElement.appendChild(headingTag);
    }
}