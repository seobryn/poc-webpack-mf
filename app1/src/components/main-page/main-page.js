import Heading from '../heading/heading';
import Button from '../test-button/test-button';

export default class MainPage {
  render(rootId) {
    const container = document.getElementById(rootId)
    new Heading().render(container,'Index');
    new Button().render(container);
  }
}
