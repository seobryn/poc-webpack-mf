const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { ModuleFederationPlugin } = require('webpack').container;

const isDevelopment = process.env.NODE_ENV !== 'production';

const plugins = !isDevelopment
  ? [
      new MiniCssExtractPlugin({
        filename: '[name].[contenthash].css',
      }),
    ]
  : [];

plugins.push(new CleanWebpackPlugin());
plugins.push(
  new HtmlWebpackPlugin({
    filename: 'dashboard.html',
    title: 'Dashboard App',
  })
);

plugins.push(
  new ModuleFederationPlugin({
    name: 'Dashboard',
    remotes: {
      MainApp : 'MainApp@http://localhost:8081/remoteEntry.js',
      LandApp : 'LandApp@http://localhost:8082/remoteEntry.js'
    }
  })
);


module.exports = {
  entry: './src/dashboard.js',
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, './dist'),
    publicPath: 'http://localhost:8080/',
  },
  mode: isDevelopment ? 'development' : 'production',
  devServer: {
    contentBase: path.resolve(__dirname, './dist'),
    index: 'dashboard.html',
    port: 8080,
    writeToDisk: true,
    historyApiFallback: {
      'index': 'dashboard.html'
    }
  },
  module: {
    rules: [
      {
        test: /.(s)?css$/,
        use: [
          isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/env'],
            plugins: ['@babel/plugin-proposal-class-properties'],
          },
        },
      },
    ],
  },
  optimization: {
    minimize: true,
    splitChunks: {
      chunks: 'all',
      minSize: 3000,
    },
  },
  plugins,
};
