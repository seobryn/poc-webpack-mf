import './navigation-bar.scss'

export default class NavigationBar {
    render(parentELement,navigationItems){
        const nav= document.createElement('ul');
        nav.classList.add('navigation-bar')
        navigationItems.forEach((item)=>{
            let itemElement = document.createElement('li');
            itemElement.classList.add('navigation-bar--item');
            itemElement.innerHTML = `<a href="${item.url}">${item.name}</a>`;
            nav.appendChild(itemElement);
        })
        parentELement.appendChild(nav);
    }
}