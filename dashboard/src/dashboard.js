import NavigationBar from './components/navigation-bar/navigation-bar.js';
import './global.scss'

const url = window.location.pathname;

const navigationItems = [
  {
    url: '/main',
    name: 'Main Page'
  },
  {
    url: '/land',
    name: 'Land Page'
  }
]

new NavigationBar().render(document.body,navigationItems);
const AppContainer = document.createElement('div')
AppContainer.id ="root";
AppContainer.classList.add('micro-frontend');
document.body.appendChild(AppContainer);


if (url === '/main') {
  import('MainApp/MainPage').then((MainPageModule) => {
    const MainPage = MainPageModule.default;
    new MainPage().render('root');
  });
}

if (url === '/land') {
  import('LandApp/LandPage').then((LandPageModule) => {
    const LandPage = LandPageModule.default;
    new LandPage().render('root');
  });
}
