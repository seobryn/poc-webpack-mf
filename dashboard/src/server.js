const express = require('express');
const app = express()
const path = require('path')
const fs = require('fs')

const PORT = 8080;

function GetFile (fileName) {
    const filePath = path.resolve(__dirname,`../dist/${fileName}.html`);
    const htmlText = fs.readFileSync(filePath,'utf-8');
    return htmlText;
}

app.use('/', express.static(path.resolve(__dirname, '../dist')))

app.get('*', (req,res)=>{
    const fileContent = GetFile('dashboard');
    res.send(fileContent);
});

app.listen(PORT, ()=>{
    console.log(`App Running on Port ${PORT}`);
})