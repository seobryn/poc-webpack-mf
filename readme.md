# Webpack 5 Module Federation Micro- Frontend Application

- This Project is a POC based on Udemy Course, it tries to implement Webpack Module Federation to Build Micro Frontends using HTML 5, JS & SASS, with an Expressjs Server to host services.

## Setup

### Prerequisites

- You have to use Nodejs 12.x + to run the app.
- You have to Install `YARN` to manage node Packages.

### Steps to Build & Run

- to setup the project, you have to run this commands for each folder:

1. Run `yarn` command in app1, app2 and dashboard to Install dependencies.
2. Run `yarn build` for each project to build dependencies.
3. Run `yarn start` for each folder to Run the micro frontends.
4. If you have to develop something inside the Project, you can use the command `yarn dev` to run webpack dev server.